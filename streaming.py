import argparse
import sys
import xmltodict
import json
import apptek_pb2
from transcribe_service import Client

def command_line():
    parser = argparse.ArgumentParser(
        prog='python streaming.py',
        description='Example streaming recognition client using AppTek ASR')
    parser.add_argument(
        '--audio_file', required=True, type=str, help='Audio file to stream')
    parser.add_argument(
        '--connect_to', required=True, type=str, help='Address to send audio packets')
    parser.add_argument(
        '--encoding',
        required=False,
        type=int,
        choices=[0, 1, 2],
        default=0)
    parser.add_argument(
        '--sample_rate',
        required=False,
        type=int,
        choices=[8000,16000],
        default=8000)
    
    return parser.parse_args()


if __name__ == '__main__':
    config = command_line()
    # check parser options
    # throw error if given options are not choosen
    print('***** config *****')
    print(config)
    print('*****')
    # aquire streaming client
    try:
        with open(config.audio_file, 'rb') as stream:
            # sample needs a .read()-supporting file-like object
            client = Client(
                connect_to=config.connect_to,
                stream=stream,
                encoding=config.encoding,
                sample_rate=config.sample_rate)
            # generator function streaming_recognize() recognizes the audio
            # and returns the results on the fly
            results = client.streaming_recognize()
            for result in results:
                if result.transcription.transcription:
                    text_json = json.dumps(xmltodict.parse(result.transcription.transcription))
                    text_json = json.loads(text_json)
                    print(text_json['results']['segment']['orth'])
                else:
                    print(result)
                print("*****")
    except Exception as ex:
        print(ex)
