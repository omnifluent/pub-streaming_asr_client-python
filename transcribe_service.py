import apptek_pb2
import apptek_pb2_grpc
import grpc
import time

class StreamObserver:
    def __init__(self):
        self.eof = False

    def set_eof(self):
        self.eof = True

    def is_eof(self):
        return self.eof

    def is_not_eof(self):
        return not self.eof


class Client:
    def __init__(self, connect_to, stream, encoding, sample_rate):
        self._connect_to = connect_to
        self._encoding = encoding
        self._sample_rate = sample_rate
        self._stream = stream

    def _get_config(self):
        return apptek_pb2.StreamRecognizeRequest(
            config=apptek_pb2.StreamRecognizeConfig(
                encoding=self._encoding,
                sample_rate=self._sample_rate,
                segmentation_enabled=True,
                speaker_change_enabled=True,
                segment_end_signal_enabled=True,
                confidence_enabled=True,
                ))
                
    def _get_audio_data(self, data):
        return apptek_pb2.StreamRecognizeRequest(audio=data)

    def _streaming(self, stream_observer):
        # send initial configuration ticket
        yield self._get_config()
        # now stream audio
        more = True
        toRead = 2048
        if self._encoding == 0:
            # read up to 250 msec of audio (16bit sample)
            toRead = int(self._sample_rate / 2)
            print('toRead: {}'.format(toRead))
        
        while more:
            data = self._stream.read(toRead)
            more = len(data) > 0 and stream_observer.is_not_eof()
            if more:
                print('sending audio packets')
                yield self._get_audio_data(data=data)
                # adding sleep just to simulate streaming
                time.sleep(0.25)               

    def streaming_recognize(self):
        if len(self._connect_to):
            channel = grpc.insecure_channel(self._connect_to)
            stub = apptek_pb2_grpc.MtpStreamAsrStub(channel)
            
            stream_observer = StreamObserver()
            
            for output in stub.StreamAudio(self._streaming(stream_observer)):
                yield output


