# Apptek Streaming Asr python client for dockerized verison.

Assuming python 3.6 or higher is installed.
    
# Evnironment Set Up

    python3 -m venv env
    source env/bin/activate
    pip install --upgrade pip
    pip install -r requirements.txt
    ./compile.sh
# Running the application
    python streaming.py --audio_file <audio location> --connect_to <streaming server address>
<audio location> = full path of 8Khz PCM wav file
<streaming server address> = hostname (or IP) and port for ASR grpc server

This client just logs the output to console if further processing is needed logic can be included to results to 'streaming.py' at line:51